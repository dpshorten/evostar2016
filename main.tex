% This is based on the LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%
% See http://www.springer.com/computer/lncs/lncs+authors?SGWID=0-40209-0-0-0
% for the full guidelines.
%
\documentclass{llncs}

\usepackage[numbers]{natbib}
\usepackage{amsmath}
\usepackage{subfig}
\usepackage{graphicx}
\usepackage{pifont}
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%
\usepackage{amssymb}

\begin{document}

\title{The Evolution of Evolvability in Evolutionary Robotics}

\titlerunning{Evolution of Evolvability}  % abbreviated title (for running head)
%                                     also used for the TOC unless
%                                     \toctitle is used
%
\author{David Shorten\inst{1} \and Geoff Nitschke\inst{2}}
%
\authorrunning{David Shorten \textbf{}\and Geoff Nitschke} % abbreviated author list (for running head)
%
%%%% list of authors for the TOC (use if author list has to be modified)
\tocauthor{David Shorten and Geoff Nitschke}
%
\institute{
  University of Cape Town, South Africa\\
	\email{dshorten@cs.uct.ac.za}
\and
	University of Cape Town, South Africa\\
	\email{gnitschke@cs.uct.ac.za}
}

\maketitle              % typeset the title of the contribution

\begin{abstract}
Previous research has demonstrated that certain models of Gene Regulatory Networks (GRNs) are capable of evolving
evolvability \cite{crombach2008evolution}\cite{draghi2008evolution}, where evolvability is taken to be the
population's \textit{responsiveness} to environmental change.  In these works the phenotypes were merely the bit
strings formed by concatenating the activations of the GRN after simulation. In this research, these previous
results are replicated, however, the phenotype space is enriched with time and space dynamics through the use of a robot
simulation environment. It was found that when a GRN encoding is used in the evolution of a way-point collection
behaviour in a fluctuating environment that populations were able to become substantially more responsive over
time. This is as compared to a direct encoding which was unable to improve. In a wall avoidance task it was observed that
the GRN did allow for responsiveness to improve over time, however the effect was smaller than in the way-point
collection task. 
\keywords{evolutionary biology, evolutionary robotics, evolvability, gene regulatory networks}
\end{abstract}
%
\section{Introduction}
\label{sec:introduction}
%
One can question whether digital and natural organisms undergoing evolution are able to become more \textit{responsive}
to changes in their environment. That is, if the environment varies over time, can they evolve the ability to be able to
evolve suitable adaptations to these changes faster? Crombach and Hogeweg \citep{crombach2008evolution} as
well as Draghi and Wagner \citep{draghi2008evolution} have demonstrated that certain models of GRNs
are able to do this. In these works, the phenotypes were merely the bit strings formed by concatenating the activations
of the GRN after simulation. The primary goal of this research is to replicate those results using a much richer 
phenotype space imbued with time and space dynamics. This space will be that of robot behaviours in two different task
environments.

The representation problem in Evolutionary Computing (EC) concerns itself with how problem solutions are represented and the intertwined
issue of how mutation and crossover operators act on these solutions. It has been found that the choice of
representation and associated operators can have an enormous impact on the performance of evolution and representations
which facilitate evolution have been termed `evolvable' \cite{wagner1996perspective, rothlauf2006representations,
  simoes2014self}.

This then begs the question of what representation is used in nature and why.  Although information concerning the form
of an organism is stored within its genome, the developmental process which translates this information into form is
incredibly complicated and not fully understood \cite{pigliucci2010genotype}. However, it has become clear that the
mapping between genotype and phenotype is neither one-to-one nor linear. In many organisms, as well as in the much
studied case of RNA folding, it has been found that many genotypes can code for a single phenotype and that the amount
of genetic change in a mutation is not proportional to the phenotypic change \cite{pigliucci2010genotype,
  wagner2008robustness, parter2008facilitated}. These mappings are further complicated by the fact that genotype space
is often of a much higher dimension, and many orders of magnitude larger, than the associated phenotype
space \cite{huynen1996smoothness, wagner2008robustness, stadler2001topology}. This has the result that the effect of
mutations is not only determined by the representation and associated mutation operators but also by the neighbourhood
of genotype space that the population inhabits. This is distinct from a one-to-one mapping where, for a given phenotype
and its associated genotype, the effects of mutations are solely determined by the representation, mutation operators
and fitness function. One can therefore choose to view the position of the population in genotype space as another facet
of the representation itself, one that can easily change during the course of evolution.

An open question in biology is whether these representations have occurred by chance, or whether they themselves have
been the subject of evolution \cite{parter2008facilitated}. That is, are evolvable representations able to evolve? Or,
put in slightly different language, is the evolution of \textit{evolvability} possible?

This question is complicated by the many different meanings of the word evolvability in both evolutionary biology
\cite{pigliucci2008evolvability} and EC \cite{taraporecomparing}. For instance, the term can refer to a property of
either a population or an individual \cite{wilder2015reconciling}. Furthermore, the meaning of the term itself is
subject to variation. Within the biological literature, a plethora of definitions exist and the reader is referred to
the excellent review by Pigliucci \cite{pigliucci2008evolvability}. Common themes in that
literature are the amount of phenotypic variation which is accessible to a genotype as well as the fitness of these
variants \cite{pigliucci2010genotype, wagner2008robustness, parter2008facilitated}. Within EC, a number of
different definitions and associated metrics have also been proposed, including those that focus exclusively on either
the fitness \cite{hornby2003generative, grefenstette1999evolvability, reisinger2007acquiring} or variability
\cite{lehman2013evolvability} of offspring. Tarapore and Mouret \cite{taraporecomparing} developed a metric which
incorporated both the fitness and diversity of offspring.  One metric, which excludes discussions of fitness and
variability, is that of Reisinger and Miikkulainen \cite{reisinger2005towards}, which measures evolvability as the ability
of organisms to detect deeper patterns in a dynamic fitness function.

In this research, a population's evolvability is defined as its \textit{responsiveness}; its ability to rapidly adapt to
changes in the fitness landscape. That is, evolvability is tantamount to a population's adaptability. Clearly, factors such
as phenotypic diversity and fitness of offspring are correlated with such a measure. However, instead of imposing a set
of features that will increase the likelihood of the production of useful phenotypes (behaviours), this
definition defers to organism's capability to adapt and thus survive in its given environment. It also has precedent
within the biological literature, \cite{pigliucci2008evolvability,flatt2005evolutionary}.

Over and above modeling a biological phenomenon, the evolution of evolvability in fluctuating environments could be
useful for solving dynamic optimization problems \cite{branke2000multi}.

The evolution of responsiveness was investigated in two robotics task domains, using both GRN and direct encodings (see
the Methods section for more details). The first robotic task involved the collection of way-points and in the second
the robot was required to avoid walls whilst still moving fast.

In order to implement environmental fluctuation, each task had two variants. In the collection task two different
way-point layouts were used and in the wall avoidance task two different wall layouts were used.

A bit-string was used for the genotype in the direct encoding. This was then translated into an ANN robot controller,
which was simulated in a 2-D robot environment in order to arrive at a fitness. In the GRN encoding, the GRN was
simulated for a predetermined number of timesteps, before the activations of the nodes were read off to form a
bit-string. This bit-string was then translated into an ANN robot controller, in the same manner as for the direct
encoding.

It was observed that during evolution on the way-point collection task that when the GRN encoding was used that that the
populations were able to become significantly more responsive to environmental fluctuations over evolutionary time. On
the other hand, the direct encoding was unable to improve over time.  However, in the wall avoidance task it was found
that the GRN did allow for responsiveness to improve over time, but the effect was much smaller than in the way-point
collection task.

\section{Methods}
\label{sec:methods}

\subsection{Simulation Environment}

A simple 2-D robotics simulation, similar to RoboRobo \cite{bredeche2013roborobo} was implemented. The simulation arena was surrounded by solid
walls. The robot was free to move within these walls. 
There were two different tasks which could be presented to the robot and each task had two variants. During evolution, the
task variants were switched in order to present a fluctuating environment, see section \ref{sec:methods:ea}. Two distinct
tasks were used in order to show that the results obtained were not due to some quirk of the specific robot task.

One task was way-point collection. In each of the two task variants the robot was required to `collect' a certain number of
way-points. It did so by passing within a 
predefined radius of the them. The way-points had to be activated in order and the robot had a limited number of timesteps in
which to operate. The number of way-points which the robot activated during its lifetime became its fitness.
Figure \ref{fig:waypoints_environment} shows the layout of the way-points for each
of the two task variants. It should be clear that the 
positioning of the way-points was constructed to encourage the emergence of a wall-following behaviour. 

The other task was wall avoidance. The robot was evolved under the fitness function (\ref{equation:fitness})
\begin{equation} \label{equation:fitness} f(v, d, \omega) = \Sigma_t v_t d_t (1 - \omega_t)  \end{equation}
where $v_t$ is the robots speed at time $t$, $\omega_t$ is its rotational speed and $d_t$ is the distance to the closest
wall. The two task variants differed in the layouts of the walls. Figure \ref{fig:avoidance_environment} shows
these two wall layouts.

\begin{figure}[bth]
  \subfloat[Task variant one]{\label{fig:waypoints_environment:task1}\includegraphics[width=.45\linewidth]{waypoints_environment_task1.png}}
  \hspace{5mm}
  \subfloat[Task variant two]{\label{fig::waypoints_environment:task1}\includegraphics[width=.45\linewidth]{waypoints_environment_task2.png}} \\
  \caption[Visualization of the way-point collection task simulation environment.]{\label{fig:waypoints_environment}  
    Visualization of the of the way-point collection task simulation environment, at the start of the simulation, for
    the each of the two task variants. The maroon circles represent the way-points and the surrounding black circles the
    activation radius. The robot is represented by the blue circle and the green lines show its sensors' directions and
    ranges.}
\end{figure}

\begin{figure}[bth]
  \subfloat[Task variant one]{\label{fig:avoidance_environment:task1}\includegraphics[width=.45\linewidth]{avoidance_environment_task1.png}}
  \hspace{5mm}
  \subfloat[Task variant two]{\label{fig::avoidance_environment:task1}\includegraphics[width=.45\linewidth]{avoidance_environment_task2.png}} \\
  \caption[Visualization of the wall avoidance task simulation environment.]{\label{fig:avoidance_environment}  
    Visualization of the of the wall avoidance task simulation environment, at the start of the simulation, for
    each of the two task variants. The robot is represented by the blue circle and the green lines show its sensors'
    directions and ranges.}
\end{figure}

\subsection{Robot Controller} 
The robots controller received two inputs. These came from two distance sensors, each placed $\pi/3$ radians on either
side of the direction in which the robot was facing. These sensors operated by casting a line in their direction and, if
this line intersected a wall within the sensor's range, then the sensor's reading was $d/r$, where $d$ is the distance to
the wall and $r$ was the sensor's range. If there was no such wall, then the sensor read $1.0$. 

Similarly, the robot was able to provide the environment with two outputs. One of these determined the speed at which the
robot traveled and the other determined its angular velocity.

Inputs were mapped to outputs through a feed-forward neural network with three neurons in its hidden layer. This
network thus had twelve weights.

\subsection{Gene Regulatory Network}
The GRN model used in this work was very similar to those used in previous work on evolvability in GRNs
\citep{crombach2008evolution, draghi2008evolution}. The nodes in the network can be viewed as genes, the connections
between the nodes as excitatory or inhibitory interactions between genes via their products and the activations of the
nodes as the presence of the gene's product in the cellular environment. The nodes are updated synchronously via
equation \eqref{grn-update}. 
\begin{equation}\label{grn-update} s_i(t + 1) = \left\{ \begin{array}{lr} 1 :& \sum_j w_{ij}s_j(t) > \theta_i\\ 
    0 :& \sum_j w_{ij}s_j(t) \leq \theta_i \end{array} \right. \end{equation}
Here $s_i(t)$ is the activation of the $i$th node at simulation iteration $t$. $w_{ij}$ is the weight of the directed
edge from the $i$th to the $j$th node. If no such edge exists then $w_{ij} = 0$. $\theta_i$ is the threshold of the
$i$th node. See table \ref{grn-parameters} for the parameters used in the GRNs. In order to facilitate the conversion of
activations into bit-strings all nodes were given a unique ID in the range $[0...l]$, where $l$ is one less than the
number of nodes. See section \ref{sec:GRN_encoding} for more details. 

\begin{table}
  \centering
  \begin{tabular}{ll} \hline
    \textbf{Parameter} & \textbf{Allowed Values} \\ \hline
    weights ($w_{ij}$) & $\{-2 \ldots 2\}$ \\
    thresholds ($\theta_{ij}$) & $\{-3 \ldots 3\}$ \\
    number of nodes & 60 \\
    number of outgoing connections per node & $\{0 \ldots 59\}$ \\
    simulation iterations (maximum $t$) & 20 \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[Allowed parameter values for GRNs]{Allowed parameter values for GRNs}  
  \label{grn-parameters}
\end{table}

\subsubsection{Mutational Operators}
In order for evolution to be simulated, mutational operators had to be defined which could operate on the networks. These
operators are listed in table \ref{grn-mutations}. The \texttt{mutate\_weight} operator was applied to the edges. For
every edge, the operator was applied with a given probability, listed in table \ref{evo-params}. On the other hand,
all the other operators acted on the nodes and were applied to each of them with a given probability, also shown in table
\ref{evo-params}. 
\begin{table}
  \centering
  \begin{tabular}{lp{7cm}} \hline
    \textbf{Operator} & \textbf{Description} \\ \hline
    \texttt{mutate\_weight} & A new value for the weight of an edge is chosen from the allowed values. \\
    \texttt{mutate\_activation} & The value of the initial activation of a node is flipped. \\
    \texttt{mutate\_threshold} & A new value for the threshold of a node is chosen from the allowed values. \\
    \texttt{add\_edge} & A new incoming edge is added to the node. It connects to a random node and has a random
    weight. \\
    \texttt{delete\_edge} & One of the node's edges is chosen at random and removed. \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[Mutational operators for GRNs]{Mutational operators for GRNs.} 
  \label{grn-mutations}
\end{table}

\subsubsection{Translation from encoding to controller}
A translation function had to be defined for converting the two encodings used into the neural network controllers.
\subsubsection{Direct encoding}
\label{sec:translation:direct}
In order to convert the sixty element binary string of the direct encoding into the twelve numbers which specify the
weights of the ANN controller, each string was split into twelve smaller strings of five elements each. These strings were then
converted into real numbers in the range $[0, 1]$ via equation \eqref{conversion}
\begin{equation} \label{conversion} 2 \left(\frac{\sum_{i \in \{0...4\}}a_i2^i}{2^5 - 1} - 0.5 \right) \end{equation}
where $a_i$ is the $i$th element of substring a. 
\subsubsection{GRN encoding}
\label{sec:GRN_encoding}
The GRN was simulated for the number of iterations specified in table \ref{grn-parameters}. If the GRN did not settle
on a point attractor then it was immediately marked for removal and no further evaluation took place. This was done in
order to maintain consistency with previous work \citep{crombach2008evolution, draghi2008evolution}, where it was done
for biological plausibility. Convergence on a point attractor was tested for by determining whether the node activations
in the last and second to last iterations were identical. If the GRN had settled on a point attractor then the
activations of the nodes were converted into a bit string, where the ordering of the activations was determined by their
unique IDs. These bit-strings were then converted into the ANN controller using the same method as used for the
direct encoding. 

\subsection{Evolutionary Algorithm}
\label{sec:methods:ea}
\begin{table}
  \centering
  \begin{tabular}{ll} \hline
    \textbf{Parameter} & \textbf{Value} \\ \hline
    population size & 1000 \\
    births and deaths per generation & 100 \\
    extermination tournament size & 4 \\
    breeding tournament size & 4 \\
    recombination & none \\
    generations per task variant switch & 200 \\
    total number of generations & 10 000 \\
    GRN weight mutation rate & 0.002 \\
    GRN node mutation rate & 0.02 \\
    Fixed encoding bit-string length & 60 \\
    \hline
  \end{tabular}
  \vspace{5mm}
  \caption[Evolutionary Algorithm Parameters]{Evolutionary Algorithm Parameters} 
  \label{evo-params}
\end{table}
\begin{table}
  \centering
    \begin{tabular}{p{1.5cm}|p{2cm}p{2cm}} \hline
      \textbf{Bit flips} & \multicolumn{2}{c}{\textsc{Probability}} \\ 
      & \textbf{Way-point collection} & \textbf{Wall avoidance} \\ \hline
      0 &      0.5088 &     0.4698  \\
      1 &      0.2466 &     0.2607  \\
      2 &      0.1068 &     0.1160  \\
      3 &      0.0471 &     0.0503  \\
      4 &      0.0231 &     0.0243  \\
      5 &      0.0139 &     0.0148  \\
      6 &      0.0103 &     0.0115  \\
      7 &      0.0087 &     0.0101  \\
      8 &      0.0076 &     0.0089  \\
      9 &      0.0065 &     0.0077  \\
      10 &      0.0053 &     0.0064  \\
      11 &      0.0042 &     0.0051  \\
      12 &      0.0032 &     0.0039  \\
      13 &      0.0023 &     0.0029  \\
      14 &      0.0017 &     0.0022  \\
      15 &      0.0012 &     0.0016  \\
      16 &      0.0009 &     0.0012  \\
      17 &      0.0006 &     0.0008  \\
      18 &      0.0004 &     0.0006  \\
      19 &      0.0003 &     0.0004  \\
      20 &      0.0002 &     0.0003  \\
      21 &      0.0001 &     0.0002  \\
      22 &      0.0001 &     0.0001  \\
      23 &      0.0000 &     0.0001  \\
      24 &      0.0000 &     0.0000  \\
      25 &      0.0000 &     0.0000  \\
    \end{tabular}
    \vspace{5mm}
    \caption[Mutation size probabilities for the direct encoding.]{Mutation size probabilities for the direct encoding. These
      probabilities were set to match the probabilities of mutation of the GRN causing the given number of bit
      flips. Probabilities were rounded to four decimal places. Note that the direct encoding was made up of sixty bit
      bit-strings, however, mutations larger than twenty-three bit flips never occurred. } 
    \label{evo-bitflips}
\end{table}
A simple evolutionary algorithm with tournament selection \cite{eiben2015introduction} was run. No crossover was used and
so all breeding was asexual. The parameters used in this algorithm are shown in table \ref{evo-params}. The following
subsections provide further details on the implementation of the GRN and fixed encodings, respectively. 
\subsubsection{GRN Encoding}
At the start of evolution, GRNs were randomly instantiated such that they conformed to the parameters displayed in table
\ref{grn-parameters}. Each generation, the GRNs in the population were simulated for the specified number of
iterations. If the GRN settled on a point 
attractor, then the activations were transformed into a neural network and this network was simulated in the
environment, otherwise the GRN was marked for removal. See section \ref{sec:GRN_encoding} for more details. The
removal of these individuals from the population occurred over and above the normal number of deaths per
generation. Furthermore, extra births would have to take place in order to maintain the population size. Thus, more
births and deaths could take place each generation than specified in table \ref{evo-params}. Preliminary testing showed
that the removal of these GRNs had a negligible effect on the evolutionary dynamics. After the GRNs had been assigned
fitness through simulation, births and deaths took place as specified above. The mutation operators specified in table
\ref{grn-mutations} were applied to every node of the child GRNs with the probability specified in table
\ref{evo-params}. This is with the exception of the \texttt{mutate\_weight} operator, which was applied to every edge of
the GRNs with a lower probability, also specified in table \ref{evo-params}.
\subsubsection{Direct Encoding}
Evolution using the direct encoding operated in a similar manner. At the start of evolution, the bit-strings in the
population were generated randomly. Each generation, the bit-strings were converted into neural network controllers as described in
section \ref{sec:translation:direct}. They were subsequently simulated in the simulation environment and assigned a
fitness value. Selection, deaths and births then occurred. In preliminary testing it was found that using a constant
mutation rate on each bit lead to performance which was substantially inferior to that of the GRN encoding. At a high
mutation rate, an individual of very high fitness was found very quickly, however, the population failed to converge
around it. On the other hand, when a lower mutation rate was used, the population was able to rapidly converge around its
most fit individuals, but it normally failed to find an individual of maximum possible fitness. In order to remedy this
situation, the Hamming distance between the associated bit strings of GRNs and their parents were recorded. This was done
over the 100 evolutionary runs used for the results in this report. The probability of the occurrence of each Hamming
distance was then calculated. The probability of numbers of bit-flips that occurred in the mutation of the direct encodings
was then set to match these results. That is, if, on average, the associated bit strings of GRNs had a Hamming distance
of $h$ from their parents with probability $p$, then when the direct encoding was mutated $h$ bits would be flipped with
probability $p$. These probabilities are listed in table \ref{evo-bitflips}. It was found, when these probabilities
were used, that the performance of the direct encoding was similar to the performance of the GRN during early
generations. See section \ref{sec:results}. 

\section{Results}
\label{sec:results}


Evolution was simulated for 10 000 generations and the task variant was switched every 200 generations. Due to the two
task domains and two encodings under investigation, four different evolutionary setups were run. Each of these setups
were run 100 times.

The results of these runs are shown in figures \ref{fig:waypoint-graphs},\ref{fig:avoidance-graphs} and
\ref{fig:zoom-avoidance-graph} as well as in tables \ref{table:results} and \ref{table:tests}. 

In the way-point collection objective, by comparing figures \ref{fig:waypoint-graphs:grn-early} and
\ref{fig:waypoint-graphs:grn-late} one can easily see that, when the GRN encoding was used, the population was able to
drastically improve its responsiveness over time. On the other hand, by comparing \ref{fig:waypoint-graphs:ga-early} and
\ref{fig:waypoint-graphs:ga-late}, one notices that when the direct encoding was used that the responsiveness of the population was
similar in both the early and late stages of evolution. Tables \ref{table:results:waypoint} and \ref{table:tests:waypoint} confirm that,
in the late stages of evolution, that the maximum and average fitnesses of populations using the GRN encoding were able
to respond more quickly to environmental change than populations using the GRN encoding in early generations and
populations using the direct encoding in late generations. Moreover, this result is significant. On the other hand, there was not a
significant difference in the response of the maximum and average fitnesses between the early and late stages of
evolution when the populations were using the direct encoding. It is interesting to note that, in the early stages of evolution, the
average fitness of populations using the direct encoding was more responsive than in populations using the GRN encoding.

The results of the wall avoidance task were somewhat different. There is very little apparent difference between the
four plots in figure \ref{fig:avoidance-graphs}. It is only when the average values are plotted alongside one another in a
magnified portion of the original plots, shown in figure \ref{fig:zoom-avoidance-graph}, that a difference is
discernible. By looking at tables \ref{table:results:avoidance} and \ref{table:tests:avoidance} one can observe that,
although the average fitness responds better in GRN encoded populations during later generations, there is not a significant difference in
the responsiveness of the population maximum fitnesses. Moreover, in the early stages of evolution, populations using the
direct encoding had significantly more responsive maximum and average fitness values. It is worth noting, however, that the
responsiveness of the GRN encoded populations did improve over evolutionary time, whereas, for the populations using
the direct encoding, a significant difference between their early and late responsiveness was not observed. 

\begin{figure}[bth]
  \subfloat[GRN, early generations.]{\label{fig:waypoint-graphs:grn-early}\includegraphics[width=.45\linewidth]{waypoint_grn_early}} \quad
  \subfloat[GRN, late generations]{\label{fig:waypoint-graphs:grn-late}\includegraphics[width=.45\linewidth]{waypoint_grn_late}} \\
  \subfloat[Direct encoding, early generations.]{\label{fig:waypoint-graphs:ga-early}\includegraphics[width=.45\linewidth]{waypoint_ga_early}} \quad
  \subfloat[Direct encoding, late generations.]{\label{fig:waypoint-graphs:ga-late}\includegraphics[width=.45\linewidth]{waypoint_ga_late}}
  \caption[Plot of way-point collection results]{\label{fig:waypoint-graphs} 
    Plots of the average and maximum fitness in the way-point collection task. One
    can observe that the GRN encoded populations perform particularly badly after the first goal change. It is
    suspected that this is due to robust GRNs being favoured during the generations before the goal
    switch.}
\end{figure}

\begin{figure}[bth]
  \subfloat[GRN, early generations.]{\label{fig:avoidance-graphs:grn-early}\includegraphics[width=.45\linewidth]{avoidance_grn_early}} \quad
  \subfloat[GRN, late generations]{\label{fig:avoidance-graphs:grn-late}\includegraphics[width=.45\linewidth]{avoidance_grn_late}} \\
  \subfloat[Direct encoding, early generations.]{\label{fig:avoidance-graphs:ga-early}\includegraphics[width=.45\linewidth]{avoidance_ga_early}} \quad
  \subfloat[Direct encoding, late generations.]{\label{fig:avoidance-graphs:ga-late}\includegraphics[width=.45\linewidth]{avoidance_ga_late}}
  \caption[Plot of wall avoidance results]{  \label{fig:avoidance-graphs} 
    Plots of the average and maximum fitness in the wall avoidance task. Note that the maximum possible fitness in the
    two task variants is different, unlike in the way-point collection task.}
\end{figure}

\begin{figure}[bth]
  \includegraphics[width=.95\linewidth]{zoom_avoidance} \quad
  \caption[Zoomed plot of wall avoidance results]{  \label{fig:zoom-avoidance-graph}
    Plot showing the average population fitness for both the GRN encoding and the DE over a small time window
    of the wall avoidance task.} 
\end{figure}

\begin{table}
  \centering
  \subfloat[Way-point collection task]{
    \label{table:results:waypoint}
    \begin{tabular}{lll} 
      \hline
      \textbf{Measure} & \textbf{Mean Value} & \textbf{Standard Deviation} \\ \hline
      early maxes GRN &        7.04  &        1.40  \\
      late maxes GRN &        7.66  &        0.91  \\
      early maxes DE &        6.88  &        1.35  \\
      late maxes DE &        6.83  &        1.39  \\
      early avs GRN &        2.61  &        0.84  \\
      late avs GRN &        3.62  &        1.31  \\
      early avs DE &        3.14  &        1.23  \\
      late avs DE &        3.08  &        1.31  \\
      \hline
    \end{tabular}
  } \\
  \subfloat[Wall avoidance task]{
    \label{table:results:avoidance}
    \begin{tabular}{lll} 
      \hline
      \textbf{Measure} & \textbf{Mean Value} & \textbf{Standard Deviation} \\ \hline
      early maxes GRN &       13.77  &        2.19  \\
      late maxes GRN &       16.19  &        0.37  \\
      early maxes DE &       15.90  &        0.81  \\
      late maxes DE &       16.14  &        0.31  \\
      early avs GRN &        8.88  &        3.42  \\
      late avs GRN &       13.60  &        1.20  \\
      early avs DE &       12.46  &        2.28  \\
      late avs DE &       13.20  &        1.10  \\
      \hline
    \end{tabular}
  }
  \vspace{5mm}
  \caption[Statistical measures computed on the experiment data.]{  \label{table:results}
    Statistical measures computed on the experiment data. Every generation the population maximum and minimum fitness
    were computed. The mean and standard deviation of these values at generation twenty-five and 425 were then
    calculated. These are referred to as `early' statistics in the table. Similar statistics were also computed on
    generations 9225 and 9625 and these are referred to as `late' statistics in the table. These generations were chosen
    as they are all only an eighth into the allotted generations for one task variant, meaning they are a good measure of the
    population`s early response to the environmental change. Moreover, in these generations evolution will always be
    towards the first task variant. Note that DE is used as an abbreviation for `direct encoding'.
  }  
\end{table}

\begin{table}
  \centering
  \small
  \subfloat[Way-point collection task]{
    \label{table:tests:waypoint}
    \begin{tabular}{lllllllll} \hline
                   & EM GRN  & LM GRN  & EM DE   & LM DE   & EA GRN  & LA GRN  & EA DE   & LA DE  \\ \hline
      EM GRN  & \textbullet  & \cmark       & \xmark       & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \textbullet \\       
      LM GRN  & \cmark       & \textbullet  & \textbullet  & \cmark       & \textbullet  & \textbullet  & \textbullet  & \textbullet \\      
      EM DE   & \xmark       & \textbullet  & \textbullet  & \xmark       & \textbullet  & \textbullet  & \textbullet  & \textbullet \\ 
      LM DE   & \textbullet  & \cmark       & \xmark       & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \textbullet \\       
      EA GRN  & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \cmark       & \cmark       & \textbullet \\       
      LA GRN  & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \cmark       & \textbullet  & \textbullet  & \cmark      \\       
      EA DE   & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \cmark       & \textbullet  & \textbullet  & \xmark      \\       
      LA DE   & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \textbullet  & \cmark       & \xmark       & \textbullet \\       
      \hline
    \end{tabular}
  } \\
  \subfloat[Wall avoidance task]{
    \label{table:tests:avoidance}
    \begin{tabular}{lllllllll} \hline
                       & EM GRN    & LM GRN    & EM DE    & LM DE    & EA GRN    & LA GRN    & EA DE    & LA DE  \\ \hline
      EM GRN      & \textbullet    & \cmark         & \cmark        & \textbullet   & \textbullet    & \textbullet    & \textbullet   & \textbullet \\       
      LM GRN      & \cmark         & \textbullet    & \textbullet   & \xmark        & \textbullet    & \textbullet    & \textbullet   & \textbullet \\      
      EM DE       & \cmark         & \textbullet    & \textbullet   & \xmark        & \textbullet    & \textbullet    & \textbullet   & \textbullet \\ 
      LM DE       & \textbullet    & \xmark         & \xmark        & \textbullet   & \textbullet    & \textbullet    & \textbullet   & \textbullet \\         
      EA GRN      & \textbullet    & \textbullet    & \textbullet   & \textbullet   & \textbullet    & \cmark         & \cmark        & \textbullet \\       
      LA GRN      & \textbullet    & \textbullet    & \textbullet   & \textbullet   & \cmark         & \textbullet    & \textbullet   & \cmark      \\       
      EA DE       & \textbullet    & \textbullet    & \textbullet   & \textbullet   & \cmark         & \textbullet    & \textbullet   & \xmark      \\       
      LA DE       & \textbullet    & \textbullet    & \textbullet   & \textbullet   & \textbullet    & \cmark         & \xmark        & \textbullet \\       
      \hline
    \end{tabular}
  }
  \vspace{5mm}
  \caption[Significance tests on the statistics in table \ref{table:results}]{\label{table:tests}
    Significance tests on the statistics in table \ref{table:results}. \cmark $\>$ signifies a statistically significant difference between the two
    quantities with $p < 0.01$ using the Mann-Whitney U test and the Bonferroni correction for multiple comparisons. \xmark $\>$ signifies that the
    difference between the two quantities is not significant and \textbullet $\>$ signifies that a test was not done. EM is an abbreviation for `early
    maxes' and, similarly, LM is an abbreviation for `late maxes'. Likewise, EA is an abbreviation for `early averages' and LA is an abbreviation for
    `late averages'. DE is an abbreviation for `direct encoding'.
  }  
\end{table}


\section{Discussion}
\label{sec:discussion}
The results demonstrate that the use of a GRN encoding makes it possible for robot populations to evolve to become
more responsive to changes in their environment. This result is significant as it extends previous work demonstrating
this result in the evolution of bit-strings \cite{crombach2008evolution}\cite{draghi2008evolution} into a much richer
environment that contains both time and space dynamics. Moreover, it acts as a proof of concept for applying these
encodings to various problems within EC. Having populations that are more responsive to changes in the task could
be of great use in dynamic optimisation problems \cite{branke2000multi}. 

Although a substantial improvement in responsiveness was observed in the way-point collection task, the size of the
effect in the wall avoidance task was very small, probably not justifying the extra computational resources required by
the GRN encoding. 

It is worthwhile to question what the reasons for this difference are. One possible explanation is that the two task variants
were too similar to one another. Indeed, if one studies the plots in figure \ref{fig:avoidance-graphs}, one notices that,
immediately after a task variant switch, the populations contain at least one highly fit individual. This implies that
certain controllers are very fit in both task variants. However, this does not explain why the average fitness, which drops
substantially after a task variant change, is not able to evolve to be able to recover much more rapidly under the GRN
encoding. Furthermore, the nature of the fitness function might be such that it is asymptotically hard to evolve highly
fit individuals. Thus, even though a given controller might be able to be fit in both task variants, the other controllers
which are only slightly fitter can be thought of as being much more evolved. 

This then raises the question of what circumstances allow for the evolution of evolvability. It is suspected that high
dimensional genotype spaces with a large amount of redundancy facilitate this process. However, in the way-point
collection task variant, only eight possible fitness values were allowed. If one chooses to view this as representing eight
possible phenotypes, then the genotype space of the direct encoding is of a much higher dimension and highly redundant. Yet, the
evolution of evolvability was not observed.

This could imply that the genotype spaces need to be of a sufficiently high dimension, which the direct encoding is below. On
the other hand, it could imply that it is some other property of these spaces which is responsible for the observed
dynamics. This view is strengthened by the fact that the GRN encoding was far less able to evolve responsiveness in
the wall avoidance task than during way-point collection. 

It is therefore proposed that a study of the features of genotype space that facilitate the evolution of evolvability is
a worthwhile endeavour. A useful abstraction for doing this would be genotype networks, where each node represents a
genotype and each edge represents a point mutation. This approach has already been successfully applied to the study of
robustness and its influence on evolvability \cite{wagner2008robustness}. Moreover, network science is an active area of
research with many useful results and existing software packages \cite{newman2010networks}.


%
% ---- Bibliography ----
%
\bibliographystyle{splncs}
\bibliography{Bibliography} 

\end{document}
